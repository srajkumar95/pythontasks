import requests

class BugzillaAPI(object):

    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.authentication = self.get_authentication()

    def _url(self, path):
        return 'https://devops-bugzilla.sonimcloud.com/attbugzilla/rest.cgi' + path

    def get_params(self, params = {}):
        param = '?token=' + self.authentication.get('token')
        for key, value in params.items():
            param += '&' + key + '=' + str(value)
        return param

    def get_authentication(self):
        urlPath = '/login'
        param = '?login=' + self.login + '&password=' + self.password
        return requests.get(self._url(urlPath + param)).json()

    def get_bugs(self, **params):
        urlPath = '/bug'
        param = self.get_params(params)
        return requests.get(self._url(urlPath + param)).json().get('bugs')

    def get_accessible_product_ids(self):
        urlPath = '/product_accessible'
        return requests.get(self._url(urlPath)).json().get('ids')

    def get_products(self):
        urlPath = '/product'
        ids = self.get_accessible_product_ids()
        param = '?ids=' + str(ids[0])
        for id in ids[1:]:
            param += '&ids=' + str(id)
        return requests.get(self._url(urlPath + param)).json().get('products')

    @staticmethod
    def display_bug(bug):
        return bug.get('id'), bug.get('product'), bug.get('status'), bug.get('assigned_to_detail').get('real_name'), bug.get('summary')

b = BugzillaAPI(login='devops1@sonimtech.com', password='devops1')
print(b.authentication)
bugs = b.get_bugs(id=111442)
print(len(bugs), 'bug with ID 111442')
for bug in bugs:
    print(b.display_bug(bug))
print()
bugs = b.get_bugs(product='MIR_Test_Master_Product2')
print(len(bugs), 'bugs of the product: MIR_Test_Master_Product2')
for bug in bugs:
    print(b.display_bug(bug))
print()
bugs = b.get_bugs(product='MIR_Test_Master_Product2', status='NEW')
print(len(bugs), 'bugs of the product: MIR_Test_Master_Product2 with status NEW')
for bug in bugs:
    print(b.display_bug(bug))
print()
bugs = b.get_bugs(product='MIR_Test_Master_Product2', bug_status='__closed__')
print(len(bugs), 'closed bugs of the product: MIR_Test_Master_Product2')
for bug in bugs:
    print(b.display_bug(bug))
print()
bugs = b.get_bugs(bug_status='__closed__', status='RESOLVED', product='XP8 Orange')
print(len(bugs), 'closed bugs of the product: XP8 Orange with the status RESOLVED')
for bug in bugs:
    print(b.display_bug(bug))
print()
products = b.get_products()
print(len(products), 'products accessible')
for product in products:
    print(product.get('name'))